package sergei.bespalov.android.intaller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by sergei on 5/15/2015.
 */
public class AdbController {
    private static final String ADB_PATH = "ADB/adb.exe";
    private static final String CMD_INSTALL = "install";

    public static final String CMD_DEVICES_LIST = "devices";

    public static String createInstallFileCmd(File file){
        return CMD_INSTALL + " "+ file.getAbsolutePath();
    }

    public static String createUpdateApkCmd(File file){
        return CMD_INSTALL + " -r "+ file.getAbsolutePath();
    }

    public static AdbController getInstance(){
        return new AdbController();
    }

    /**
     * execute command for ADB
     * @param cmd command
     * @return return InputStream of process
     */
    public InputStream executeCmd(String cmd){
        Runtime runtime = Runtime.getRuntime();
        try {
            Process process = runtime.exec(ADB_PATH + " " + cmd);
            return process.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return list of device's serial
     */
    public   LinkedList<String> getDevicesSerialsList(){
        Scanner scanner = new Scanner(executeCmd(CMD_DEVICES_LIST));
        LinkedList<String> list = new LinkedList();
        while (scanner.hasNextLine()){
            String line = scanner.nextLine();
            if (!line.contains("devices") && line.contains("device")){
                String serial = line.substring(0,line.indexOf('\t'));
                list.push(serial);
            }
        }
        return list;
    }

    /**
     * get device name
     * @param serial device serial
     * @return device name or "device not found"
     */
    public String getDeviceName(String serial){
        String command = "-s " + serial + " shell getprop ro.product.model";
        Scanner scanner = new Scanner(executeCmd(command));
        if (scanner.hasNextLine()){
            return scanner.nextLine();
        }else return "device not found";
    }
}
