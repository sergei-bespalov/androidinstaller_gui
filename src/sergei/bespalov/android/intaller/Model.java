package sergei.bespalov.android.intaller;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergei on 5/15/2015.
 */
public class Model {
    private static File apkFileDir = new File("APK");
    private static File[] apks;

    public static String getApkPath(){
        return apkFileDir.getAbsolutePath();
    }
    public static String getApkFileDir(){
        return apkFileDir.getName();
    }

    public static void init(){
        if (apks != null) return;
        if (!apkFileDir.exists()){
            apkFileDir.mkdir();
        }
        if(!apkFileDir.isDirectory()) return;
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.getName().endsWith(".apk")) return true;
                else return false;
            }
        };
        apks = apkFileDir.listFiles(filter);
    }

    public static int getApkCount(){
        if (apks == null) return 0;
        return apks.length;
    }

    public static void changeFolder(String folder){
        apks = null;
        apkFileDir = new File(folder);
        init();
    }

    public static ArrayList<File> getAllDirectories(){
        ArrayList<File> list = new ArrayList<>();
        File[] arr = new File(".").listFiles();
        for (int i = 0; i < arr.length; i++){
            if (arr[i].isDirectory()){
                list.add(arr[i]);
            }
        }
        return list;
    }

    public static File[] getFileList(){
        return apks;
    }
}
