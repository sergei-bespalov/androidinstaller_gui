package sergei.bespalov.android.intaller;

import java.io.File;
import java.io.FileFilter;

/**
 * Created by sergei on 5/15/2015.
 */
public class Main {

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                View.createAndShowGUI();
            }
        });

    }
}
