package sergei.bespalov.android.intaller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by sergei on 5/15/2015.
 */
public class View {

    public static final AdbController mAdbController = AdbController.getInstance();
    public static final HashMap<String,JCheckBox> filesMap = new HashMap<>();
    private static boolean flagPressed = false;

    public static void addComponentsToPane(Container paneRoot) {

        paneRoot.setLayout(new BoxLayout(paneRoot, BoxLayout.X_AXIS));

        // folder list panel
        JPanel folderListPanel = new JPanel();
        folderListPanel.setLayout(new BoxLayout(folderListPanel, BoxLayout.Y_AXIS));

        JLabel titleFolders = new JLabel("\u0412\u044b\u0431\u043e\u0440 \u043f\u0430\u043f\u043a\u0438");
        titleFolders.setFont(new Font("Arial", Font.PLAIN, 12));
        titleFolders.setAlignmentX(Component.LEFT_ALIGNMENT);
        titleFolders.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        folderListPanel.add(titleFolders);


        paneRoot.add(folderListPanel);

        // left panel
        JPanel pane = new JPanel();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        JLabel title;
        String bad = new String("\u041f\u0443\u0442\u044c \u043a apk: " + Model.getApkPath());
        title = new JLabel(bad);
        title.setFont(new Font("Arial", Font.PLAIN, 12));
        title.setAlignmentX(Component.LEFT_ALIGNMENT);
        title.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        pane.add(title);

        //найдено файлов
        JLabel apkCount = new JLabel("\u041d\u0430\u0439\u0434\u0435\u043d\u043e \u0444\u0430\u0439\u043b\u043e\u0432: " + Model.getApkCount());
        apkCount.setFont(new Font("Arial", Font.PLAIN, 12));
        apkCount.setAlignmentX(Component.LEFT_ALIGNMENT);
        apkCount.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        pane.add(apkCount);

        //устройства
        JPanel jPanelDevice = new JPanel();
        jPanelDevice.setLayout(new BoxLayout(jPanelDevice, BoxLayout.X_AXIS));
        JLabel deviceLabel = new JLabel("Device: - ");
        jPanelDevice.add(deviceLabel);
        JButton updateButton = new JButton();
        updateButton.setIcon(new ImageIcon(View.class.getResource("ic_update.png")));
        updateButton.setPressedIcon(new ImageIcon(View.class.getResource("ic_update_pressed.png")));
        updateButton.setBorderPainted(false);
        updateButton.setContentAreaFilled(false);
        updateButton.setOpaque(false);
        updateButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        LinkedList<String> deviceList = mAdbController.getDevicesSerialsList();
                        String deviceName;
                        if (deviceList.size() > 0) {
                            deviceName = mAdbController.getDeviceName(deviceList.getFirst());
                        }else deviceName = "device not found";
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                deviceLabel.setText("Device: " + deviceName);
                            }
                        });
                    }
                }).start();
            }
        });
        jPanelDevice.add(updateButton);
        jPanelDevice.setAlignmentX(Component.LEFT_ALIGNMENT);
        jPanelDevice.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        pane.add(jPanelDevice);

        JPanel startButtonPanel = new JPanel();
        startButtonPanel.setLayout(new BoxLayout(startButtonPanel, BoxLayout.X_AXIS));
        JButton button = new JButton("Start");
        button.setAlignmentX(Component.LEFT_ALIGNMENT);
        startButtonPanel.add(button);

        JCheckBox needUpdate = new JCheckBox("\u041e\u0431\u043d\u043e\u0432\u0438\u0442\u044c");
        startButtonPanel.add(needUpdate);

        startButtonPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        startButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        pane.add(startButtonPanel);

        JLabel logLabel = new JLabel("Log:");
        apkCount.setFont(new Font("Arial", Font.PLAIN, 12));
        logLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        logLabel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        pane.add(logLabel);

        JTextArea logArea = new JTextArea(20, 50);
        JScrollPane scroll = new JScrollPane(logArea,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setAlignmentX(Component.LEFT_ALIGNMENT);
        pane.add(scroll);

        JLabel progress = new JLabel("not started");
        pane.add(progress);

        paneRoot.add(pane);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (flagPressed) return;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        flagPressed = true;
                        ArrayList<File> list = View.getFileList();
                        for (int i = 0; i < list.size(); i++) {
                            File file = list.get(i);
                            String cmd = needUpdate.isSelected()? AdbController.createUpdateApkCmd(file):AdbController.createInstallFileCmd(file);
                            InputStream is = mAdbController.executeCmd(cmd);
                            Scanner scanner = new Scanner(is);
                            while (scanner.hasNextLine()) {
                                String newS = scanner.nextLine();
                                String count = String.valueOf(i + 1);
                                SwingUtilities.invokeLater(new Runnable() {
                                    public void run() {
                                        String old = logArea.getText();
                                        logArea.setText(old + "\n" + newS);
                                        progress.setText("Progress " + count + "/" + list.size());
                                    }
                                });
                            }
                        }
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                progress.setText("DONE!");
                            }
                        });
                        flagPressed = false;
                    }
                }).start();
            }
        });

        //right panel
        pane = new JPanel();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        //выберите файл
        JLabel title2 = new JLabel("\u0412\u044b\u0431\u043e\u0440 \u0444\u0430\u0439\u043b\u043e\u0432");
        title2.setFont(new Font("Arial", Font.PLAIN, 12));
        title2.setAlignmentX(Component.LEFT_ALIGNMENT);
        title2.setAlignmentY(Component.TOP_ALIGNMENT);
        pane.add(title2);

        //добавить список checkbox для файлов
        JPanel checkBoxesPanel = new JPanel();
        checkBoxesPanel.setLayout(new BoxLayout(checkBoxesPanel, BoxLayout.Y_AXIS));
        File[] fileList = Model.getFileList();
        for (int i = 0; i < fileList.length; i++) {
            JCheckBox checkBox = new JCheckBox();
            checkBox.setText(fileList[i].getName());
            checkBoxesPanel.add(checkBox);
            filesMap.put(fileList[i].getAbsolutePath(),checkBox);
        }
        checkBoxesPanel.setAlignmentY(Component.TOP_ALIGNMENT);
        checkBoxesPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        JScrollPane scrollPane = new JScrollPane(checkBoxesPanel);
        scrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
        scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        pane.add(scrollPane);

        paneRoot.add(pane);

        //add radio buttons for folders
        JPanel radioButtonsPanel = new JPanel();
        radioButtonsPanel.setLayout(new BoxLayout(radioButtonsPanel, BoxLayout.Y_AXIS));
        radioButtonsPanel.setAlignmentY(Component.TOP_ALIGNMENT);
        radioButtonsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        JScrollPane scrollPane1 = new JScrollPane(radioButtonsPanel);
        ButtonGroup buttonGroup = new ButtonGroup();
        for (File x: Model.getAllDirectories()){
            JRadioButton jRadioButton = new JRadioButton();
            jRadioButton.setText(x.getName());
            if (x.getName().equals(Model.getApkFileDir())){
                jRadioButton.setSelected(true);
            }
            buttonGroup.add(jRadioButton);
            radioButtonsPanel.add(jRadioButton);
            jRadioButton.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Model.changeFolder(jRadioButton.getText());
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    title.setText("\u041f\u0443\u0442\u044c \u043a apk: " + Model.getApkPath());
                                    apkCount.setText("\u041d\u0430\u0439\u0434\u0435\u043d\u043e \u0444\u0430\u0439\u043b\u043e\u0432: " + Model.getApkCount());
                                    checkBoxesPanel.removeAll();
                                    filesMap.clear();
                                    File[] fileList = Model.getFileList();
                                    for (int i = 0; i < fileList.length; i++) {
                                        JCheckBox checkBox = new JCheckBox();
                                        checkBox.setText(fileList[i].getName());
                                        checkBoxesPanel.add(checkBox);
                                        filesMap.put(fileList[i].getAbsolutePath(),checkBox);
                                    }
                                }
                            });
                        }
                    }).start();
                }
            });
        }
        folderListPanel.add(scrollPane1);

    }


    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static void createAndShowGUI() {
        //Create and set up the window.
        Model.init();
        JFrame frame = new JFrame("Apk Installer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());

        //Display the window.
        frame.pack();
        frame.setSize(1000, 500);
        frame.setVisible(true);
    }

    private static ArrayList<File> getFileList(){
        ArrayList<File> files = new ArrayList<>();
        for(String filePath: filesMap.keySet()){
            if(filesMap.get(filePath).isSelected()){
                files.add(new File(filePath));
            }
        }
        return files;
    }

}
